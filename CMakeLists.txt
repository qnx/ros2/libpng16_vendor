cmake_minimum_required(VERSION 3.5)

project(libpng16_vendor)

find_package(ament_cmake REQUIRED)

set(PACKAGE_VERSION "1.0.0")

macro(build_libpng16)
  set(configure_opts)
  
  if(CMAKE_SYSTEM_PROCESSOR STREQUAL "x86_64")
    set(TARGET "--host=x86_64-pc-nto-qnx7.1.0")
  elseif(CMAKE_SYSTEM_PROCESSOR STREQUAL "aarch64le")
    set(TARGET "--host=aarch64-unknown-nto-qnx7.1.0")
  elseif(CMAKE_SYSTEM_PROCESSOR STREQUAL "armv7")
    set(TARGET "--host=arm-unknown-nto-qnx7.1.0eabi")
  else()
    message(FATAL_ERROR "invalid cpu CMAKE_SYSTEM_PROCESSOR:${CMAKE_SYSTEM_PROCESSOR}")
  endif()
  
  list(APPEND configure_opts "${TARGET}")
  list(APPEND configure_opts "--prefix=${CMAKE_INSTALL_PREFIX}")

  include(ExternalProject)
  ExternalProject_Add(libpng16
    URL https://sourceforge.net/projects/libpng/files/libpng16/1.6.37/libpng-1.6.37.tar.gz
    TIMEOUT 600
    CONFIGURE_COMMAND cd ${CMAKE_CURRENT_BINARY_DIR}/libpng16-prefix/src/libpng16 && ./configure "${configure_opts}"
    PATCH_COMMAND patch contrib/libtests/pngvalid.c < ${CMAKE_CURRENT_SOURCE_DIR}/pngvalid.patch
    BUILD_COMMAND cd ${CMAKE_CURRENT_BINARY_DIR}/libpng16-prefix/src/libpng16 && make
    INSTALL_COMMAND cd ${CMAKE_CURRENT_BINARY_DIR}/libpng16-prefix/src/libpng16 && make install
  )

endmacro()

find_package(libpng16 QUIET)
if(NOT libpng16_FOUND)
  build_libpng16()
endif()

ament_package()
